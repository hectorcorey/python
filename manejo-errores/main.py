#captura excepcione y manejar errores en codigo

#susceptible a fallos/ errores
"""
try:
    nombre  = input("¿Cual es tu nombre?")
    if len(nombre) > 1:
        nombre_usuario = "El Nombre es "+ nombre

    print(nombre_usuario)
except:
    print("Ha ocurriod un error, mete bien el nombre")
else:
    print("Todo funciona correctamente")
finally:
    print("Fin de la iteracion!!")
"""

"""
try:
    numero = int(input("Numero para eleva al cuadrado: "))
    print("El cuadrado es:" + str(numero*numero))
except TypeError:
    print("Debes de convertir tus cadenas a enteros")
#except ValueError:
#    print("Introduce un numero correcto")
except Exception as e:
    print(type(e))
    print("Ha ocurrido un error", type(e).__name__)
"""

#Lanzar excepciones
try:     
    nombre = input("introduce el nombre: ")
    edad= int(input("introduce la edad"))

    if edad< 5 or edad > 110:
        raise ValueError("La edad introducida no es real")
    elif len(nombre) <= 1:
        raise ValueError("El nombre no esta completo")
    else:
        print(f"Bienvenido al master en python {nombre}")
except ValueError:
    print("Introduce los datos correctamente")
except Exception as e:
    print("Eciste un error: ", e)


