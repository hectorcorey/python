
"""
Listas (arrays)
son colecciones o conjuntos de datos o valores, bajo un unico nombre
"""
#definir lista
peliculas = ["batman", "spiderman", "el señor de los anillos"]
cantantes = list(('2pac','Drake',"jennifer Lopez"))
years = list(range(2020,2050))
variada = ["Victor", 30,4.4, True, "Texto"]

"""
print(peliculas)
print(cantantes)
print(years)
print(variada)
"""

##indices

peliculas[1] = "Gran torino"
peliculas[2] = "Hobbit"
print(peliculas[1])
print(peliculas[-2])
print(cantantes[1:3])
print(peliculas[1:])


#añadir

cantantes.append("kase O")

cantantes.append("Natos y waor")
print(cantantes)

#recorrer lista

print("******************")
nueva_pelicula = ""
"""while nueva_pelicula != "parar":
    nueva_pelicula = input("Introduce la nueva pelicula: ")
    if nueva_pelicula != "parar":
        peliculas.append(nueva_pelicula)
print("***********lista de pelidculas********")
for pelicula in peliculas:
    print(f"{peliculas.index(pelicula)+1} {pelicula}")
"""

#Listas multidimencionales

print("*************Listado de contactos************************")

contactos = [
    [
        'Antonio',
        'antonio@antonio.com'
    ],
    [
        'Luis',
        'luis@luis.com'
    ],
     [
        'Salvador',
        'salvador@salvador.com'
    ]
]

##print(contactos[1][1])

for contacto in contactos:
    for elemento in contacto:
        if contacto.index(elemento) == 0:
            print("Nombre: " + elemento)
        else:
            print("Email: " + elemento)
            #print(elemento)
    print("\n")

