#Programación orientada a objetos

#Definir una clase(molde para crear mas objetos de ese tipo)
#(Coche) con caracteristicas similares

class Coche:
    #atributos o propiedades
    color = "Rojo"
    marca = "Ferrari"
    modelo = "Aventador"
    velocidad = 300
    caballaje = 500
    plazas = 2

    #metodos
    def setColor(self,color):
        self.color = color

    def getColor(self):
        return self.color

    def setModelo(self,modelo):
        self.modelo = modelo
    
    def getModelo(self):
        self.modelo

    def acelerar(self):
        self.velocidad += 1

    def frenar(self):
        self.velocidad -= 1
    
    def getVelocidad(self):
        return self.velocidad

#Fin definicio de la clase

#Crear objeto / intanciar la clase

coche = Coche()
coche.setColor("amarillo")
print(coche.marca, coche.color)
print("velocidad actual:", coche.getVelocidad())

coche.acelerar()
coche.acelerar()
coche.acelerar()
coche.acelerar()
coche.acelerar()

coche.frenar()

print("velocidad nueva:", coche.getVelocidad())





