"""
Funciones

Una función de instrucciones agrupadas bajo
un nombre concreto que puede reutilizarse invocando a
la funcióntantas veces como necesario.

def nombreDeMiFuncion(parametros):
    #Blaque / Conjunto de instrucciones

nombreDeMiFuncion(miParametro)
"""

print("##### Ejempl 1 ######")


#definir funcion
def muestraNombre():
    print("HEctor Lopez ")
    print("Nestor ")
    print("\n")

#invocar funciones

muestraNombre()

# ejemplo 2 parametros

print("##### Ejemplo 2 ######")

def mostrarTuNombre(nombre):
    print(f"Tu nombre es: {nombre}")

nombre = "hola" #input("Introduce tu nombre")

mostrarTuNombre(nombre)

print("##### Ejemplo 2 ######")

def tabla (numero):
    print(f" Tabla de multiplicar del numero: {numero}")
    for contador in range(11):
        operacion = numero * contador
        print(f"{numero} x {contador} = {operacion}")

    print("\n")

tabla(5)
tabla(7)
tabla(12)

#ejemplo


for numero_tabla in range(1,11):
    tabla(numero_tabla)

#ejemplo 4
#parametros opcionales
print("##### Ejemplo 4 ######")

def getEmpleado(nombre, dni = None):
    print("Empleado")
    print(f"Nombre: {nombre}")
    if dni != None:
        print(f"DNI: {dni}")

getEmpleado("Hector lopez","adsdasdasd")


#Ejemplo 5 return
print("##### Ejemplo 5 ######")
def saludame(nombre):
    saludo = f"Hola, saludos {nombre}"
    return saludo

print(saludame("Hector Lopez"))

print("\n##### Ejemplo 6 ######")

def calculadora(numero1, numero2, basicas = False):
    suma = numero1 + numero2
    resta = numero1 - numero2
    multi = numero1 * numero2
    division = numero1 / numero2

    cadena =""
    if basicas != False:
        cadena += "Suma: " +str(suma)
        cadena += "\n"
        cadena += "Resta: " + str(resta)
        cadena += "\n"
    else:
        cadena += "Multiplicacion: " +str(multi)
        cadena += "\n"
        cadena += "Division: " +str(division)
        cadena += "\n"

    return cadena

print(calculadora(5,5))




print("\n##### Ejemplo 7 ######")


def getNombre (nombre):
    texto = f"El Nombre es: {nombre}"
    return texto

def getApellidos (apellidos):
    texto = f"Los apellidos son {apellidos}"
    return texto

def devuelveTodo(nombre,apellidos):
    texto =  getNombre(nombre)+ "\n"+ getApellidos(apellidos)
    return texto

print(devuelveTodo("Hector2", "Lopez2"))


print("\n##### Ejemplo 8 ######")

##lambda

dime_el_year = lambda year: f"El año es {year*50}"

print(dime_el_year(2034))



