nombre = "Hector Lopez"

##Funciones generales

print(type(nombre))

#detectar tipado

comprobar = isinstance(nombre,int)

if comprobar :
    print("Es un string")
else:
    print("no Es un string")

if not isinstance(nombre,float):
    print("La variable no es un numero con decimales")

#Limpiar espacios de string

frase ="      mi contenido        "

print(frase.strip())

##Eliminar variables

year = 20022
  
print(year)

del year

#print(year)

#comprobar varianle vacia

texto = " ff "

if len(texto) <= 0 :
    print("La variable esta vacia")
else:
    print("La variable tiene:",len(texto))


#encontra caracteres

frase = "LA vida es bella"

print(frase.find("vida"))

##Remplazar palabras

nueva_frase =  frase.replace("vida","moto")

print(nueva_frase)

### mayuscilas y minisculas

print(nombre)
print(nombre.lower())
print(nombre.upper())

