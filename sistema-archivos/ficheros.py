from io import open
import pathlib
import shutil
#abrir archivo
#print(str(pathlib.Path().absolute()))

archivo = open(str(pathlib.Path().absolute())+"/sistema-archivos/fichero_texto.txt","a+")

#Escrivir dentro de un archivo

archivo.write("***********Soy un Texto metido desde pythom++++++++++++++++++++\n")
#cerrar archivo
archivo.close()


archivo_lectura = open(str(pathlib.Path().absolute())+"/sistema-archivos/fichero_texto.txt","r")

#leer contenido

#contenido = archivo_lectura.read()

#print(contenido)
#for elemento in contenido:
    #print(elemento)

#leer contenido y guardarlo en lista

lista = archivo_lectura.readlines()

archivo_lectura.close()
print(lista)

for frase in lista:
    #lista_frase = frase.split()
    #print(lista_frase)
    print("- "+frase.center(10))


    #copiar archivo
"""
ruta_original = str(pathlib.Path().absolute()) + "/sistema-archivos/fichero_texto.txt"
ruta_nueva = str(pathlib.Path().absolute()) + "/sistema-archivos/fichero_copiado.txt"
ruta_alternativa = "./bucle/fichero_copiado.txt"
shutil.copyfile(ruta_original,ruta_alternativa)
"""
#mover
"""
ruta_original = str(pathlib.Path().absolute()) + "/sistema-archivos/fichero_copiado.txt"

ruta_nueva = str(pathlib.Path().absolute()) + "/sistema-archivos/fichero_copiado_NUEVO.txt"

shutil.move(ruta_original,ruta_nueva)
"""

#eliminar archivo

import os
"""
ruta_nueva = str(pathlib.Path().absolute()) + "/sistema-archivos/fichero_copiado_NUEVO.txt"
os.remove( ruta_nueva)
"""
#comprobar si archivo existe

import os.path

print(os.path.abspath("./"))

if os.path.isfile(os.path.abspath("./") + "/sistema-archivos/fichero_texto55.txt"):
    print("El archivo Existe")
else:
    print("El Archivo no existe")

