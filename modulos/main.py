"""
Modulos
funcionalidades que se pueden reutilizar

"""
##Importar modulo propio
#import mimodulo

#from mimodulo import holaMundo

from mimodulo import *


print(holaMundo("hector"))

print(calculadora(3,5,True))


#modulo de fechas

import datetime

print(datetime.date.today())

fecha_completa =  datetime.datetime.now()
print(fecha_completa)

print(fecha_completa.year)

print(fecha_completa.day)

fecha_personalizada = fecha_completa.strftime("%d/%m/%Y, %H:%M:%S")

print("Mi fecha perzonalizada:"+ fecha_personalizada)
print(datetime.datetime.now().timestamp())

#Modulo Matematicas


import math

print("La raiz cuadrada de 10: ",math.sqrt(10))

print("Numero Pi",float(math.pi))

print("Redondear",math.floor(6.66798))


#Modulo ramdom
import random
print("Numero Aleatorio", random.randint(15,67))

