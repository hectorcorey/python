"""
Diccionario es un tipo de dato que almacena un conjunto de datos.
en el formato clave valos
"""

persona = {
    "nombre": "vitor",
    "apellidos": "Robles",
    "web": "url.com"

}

print(persona["web"])


#Lista con diccionarios


contactos =[
    {
        "nombre":"antonio",
        "email":"Antonio@antonio.com"
    },
    {
        "nombre":"luis",
        "email":"luis@luis.com"
    },
    {
        "nombre":"slavador",
        "email":"slavador@slavador.com"
    }

]


print(contactos[0]["nombre"])


print("Listado de contactos")

for contacto in contactos:
     print(f"Nombre del contacto: {contacto['nombre']}")
     print(f"Email del contacto: {contacto['email']}")
     print("----------------------------------------------")
