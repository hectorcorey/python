print('###################Condicionales######################3')

color = "rojo"
#color = input("¿Adivina cual es mi color favorito?")
if color == "rojo":
    print('El color es rojo')
else:
    print("Color incorrecto")

"""
operadores de comparacion
== igual
!= diferente
<menor que
>mayor
<= menor igual
>= mayor igual
"""

print("######Ejemplo 2#######")

year = 2021
#year = int(input("¿Que año es?"))

if year >= 2021:
    print("estamos en 2021 en adelante")
else:
    print("es un año menor a 2021")



print("######Ejemplo 3#######")

nombre = "Hector lopez"
ciudad = "Mexico"
continente = "Europa"
edad = 18
mayoria_edad = 18

if edad >= mayoria_edad:
    print(f"{nombre} es mayor de edad !!")
    if continente != "America":
        print("El usuario no es Americano")
    else:
        print(f"Es Americano y de {ciudad}")
else:
    print(f"{nombre} No es mayor de edad")


print("###### Ejemplo 4 #######")

dia = 1
#dia = int(input("¿Introduce el numero del dia de la semana:"))
if dia == 1:
    print("Es Lunes")
elif dia ==2:
    print("Es Martes")
elif dia ==3:
    print("Es Miercoles")
elif dia ==4:
    print("Es Jueves")
elif dia ==5:
    print("Es Viernes")
elif dia ==6:
    print("Es Sabado")
elif dia ==7:
    print("Es Domingo")

print("###### Ejemplo 5 #######")
"""
operadores logicos

and Y
or O
! negacion
not no
"""
edad_minima = 18
edad_maxima = 65
edad_oficial = 10

if edad_oficial >= edad_minima and edad_oficial <= edad_maxima:
    print("Esta en edad de trabajar")
else:
    print("No esta en edad de tranajar")

print("###### Ejemplo 6 #######")

pais = "España"

if pais == "Mexico" or pais == "Colombia" or pais == "España":
    print(f"{pais} es un pais de habla hispana !!")
else:
    print(f"{pais} no es un pais de habla hispna :(")

print("###### Ejemplo 7 #######")

pais = "España"

if not(pais == "Mexico" or pais == "Colombia" or pais == "España"):
    print(f"{pais} no es un pais de habla hispna :(")
else:
    print(f"{pais} es un pais de habla hispana !!")

print("###### Ejemplo 8 #######")

pais = "Colombia"

if pais != "Mexico" and pais != "Colombia" and pais != "España":
    print(f"{pais} no es un pais de habla hispna :(")
else:
    print(f"{pais} es un pais de habla hispana !!")


