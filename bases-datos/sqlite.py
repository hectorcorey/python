#importar modulo

import sqlite3

#conexión

conexion = sqlite3.connect('pruebas.db')
#crear cursor
cursor = conexion.cursor()

#crear tabla
cursor.execute("CREATE TABLE IF NOT EXISTS productos("+
"id INTEGER PRIMARY KEY AUTOINCREMENT, "
"titulo VARCHAR(255), "+
"descripcion TEXT, "+
"precio int(255)"+
")")

#Guardar cambios
conexion.commit()
#insertar datos

"""
cursor.execute("INSERT INTO productos VALUES (null,'Producto 1','Descripciondel mi producto', 550)")
conexion.commit()
"""

#cursor.execute("DELETE FROM productos")
#conexion.commit()

#Insertar muchos registros de golpe
"""
productos = [
    ("Ordenador portatil", "Buen portatil",700),
    ("Telefono movil", "Buen Telefono",140),
    ("Placa base", "Buen placa",80),
    ("Tablet 15", "Buen tablet",300)
]

cursor.executemany("insert into productos values(null,?,?,?)", productos)
conexion.commit()
"""

#update
cursor.execute("update productos set precio =678 where precio = 80")
conexion.commit()
#listar datos
cursor.execute("SELECT * FROM productos where precio >= 300")
productos = cursor.fetchall()
for producto in productos:
    print("ID:",producto[0])
    print("Titulo: ",producto[1])
    print("Descripcion: ",producto[2])
    print("Precio: ",producto[3])
    print("\n")

cursor.execute("SELECT titulo FROM productos")
producto = cursor.fetchone()
print(producto)
#Cerra conexion
conexion.close()