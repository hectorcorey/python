import mysql.connector


#conexion

database = mysql.connector.connect(
    host ="localhost",
    user= "root",
    passwd="divet",
    database ="prueba"
)

#print(database)

cursor = database.cursor(buffered=True)

"""
cursor.execute("CREATE DATABASE IF NOT EXISTS masterpython")
cursor.execute("show databases")
for bd in cursor:
    print(bd)
"""

#Crear tablas

cursor.execute("""
create table if not exists vehiculos(
id int(10) auto_increment not null,
marca varchar(40) not null,
modelo varchar(40) not null,
precio float(10,2) not null,
constraint pk_vehiculo primary key(id)
)
""")

cursor.execute("show tables")

for table in cursor:
    print(table)

#
# 
# cursor.execute("insert into vehiculos values(null,'opel','Astra',18500)")

coches= [
    ('seat','ibiza',5000),
    ('Renault','clio',15000),
    ('citroen','saxo',2000),
    ('mercedes','clase c',35000)
]

#cursor.executemany("insert into vehiculos values(null,%s,%s,%s)", coches)


database.commit()


cursor.execute("select * from vehiculos")

result = cursor.fetchall() 

print("---------Todos mis coches---------------")
for coche in result:
    print(coche[1],coche[2], coche[3])

cursor.execute("select * from vehiculos")

coche = cursor.fetchone()
print(coche)

#borrar
cursor.execute("delete from vehiculos where marca = 'Mercedes'")
database.commit()

print(cursor.rowcount, "borrados!!")

#actualizar

cursor.execute("update vehiculos set modelo='Leon' where marca='seat'")
database.commit()
print(cursor.rowcount, "actualizados!!")